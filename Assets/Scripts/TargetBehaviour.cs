﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for updating Target's position after UI button is pressed.
/// </summary>
public class TargetBehaviour : MonoBehaviour
{
    void Start()
    {
        EventsManager.Instance.OnDistanceChanged.AddListener(UpdatePosition);
    }

    private void UpdatePosition(float distance)
    {
        transform.localPosition = new Vector3(distance,0,0);
    }
}
