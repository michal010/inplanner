﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for rotating explosion animation efect since it's in 2d.
/// </summary>
public class Explosion : MonoBehaviour
{
    public Transform arCam;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = transform.GetChild(0).GetComponent<Animator>();
        EventsManager.Instance.OnCannonFired.AddListener(OnCannonFired);
    }

    public void OnCannonFired()
    {
        animator.SetTrigger("Fire");
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(arCam);
    }
}
