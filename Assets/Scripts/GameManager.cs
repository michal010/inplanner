﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class contains all essential gameplay variables.
/// </summary>
public class GameManager : MonoBehaviour
{
    public float Angle;
    public float Power;
    public float Distance;
    public int Health;
    public int MaxHealth;
    public int Score;
    public int HighScore;
    public static GameManager Instance;

    private void Start()
    {
        if(Instance == null)
        {
            Instance = this;
            MaxHealth = 3;
            Health = MaxHealth;
        }
        else
        {
            Destroy(this);
        }
    }
}
