﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class responsible for calculating balistic trajectory of an object
/// </summary>
public class BallisticTrajectory
{
    //initial velocity
    private Vector2 v0;

    //velocity
    private Vector2 v;  

    //angle of lanuch
    private float a;

    //gravity const
    private float g;

    //Starting position
    private Vector2 pos;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="v0">initial velocity of an object</param>
    /// <param name="a">angle of lanuch</param>
    /// <param name="pos">initial position of an object</param>
    public BallisticTrajectory(Vector2 v0, float a, Vector2 pos)
    {
        this.v0 = v0;
        this.a = a;
        this.pos = pos;
        g = Mathf.Abs( Physics2D.gravity.y);
    }

    /// <summary>
    /// Gets balistic trajectory position based on given time
    /// </summary>
    /// <param name="t">time</param>
    /// <returns></returns>
    public Vector2 GetPositionAtTime(float t)
    {
        float x = pos.x + (v0 * Mathf.Cos(Mathf.Deg2Rad * a)).x * t;
        float y = pos.y + (v0 * Mathf.Sin(Mathf.Deg2Rad * a)).y * t - g * t * t / 2;
        //float y = x * (v0 * Mathf.Sin(a))/ (v0 * Mathf.Cos(a)) - g * ( x / v0 * Mathf.Cos(a))^2 / 2 ;

        return new Vector2(x, y);
    }
}
