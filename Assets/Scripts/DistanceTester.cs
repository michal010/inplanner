﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for debbuging Balistic Trajecoty
/// </summary>
public class DistanceTester : MonoBehaviour
{
    public Transform t1;
    public Transform t2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    float distance;
    // Update is called once per frame
    void Update()
    {
        distance = Vector2.Distance(t1.position, t2.position);
        Debug.Log(distance);
    }
}
