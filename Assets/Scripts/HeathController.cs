﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for displaying and updating health.
/// </summary>
public class HeathController : MonoBehaviour
{
    public static HeathController Instance;
    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            EventsManager.Instance.OnCannonFired.AddListener(DecreaseHealth);
            EventsManager.Instance.OnHealthChanged.AddListener(OnHealthChanged);
        }
        else
        {
            Destroy(this);
        }
    }
    /// <summary>
    /// Checks if player has any health points left.
    /// </summary>
    /// <returns></returns>
    public bool HasHealthLeft()
    {
        return GameManager.Instance.Health > 0 ? true : false;
    }

    public void ResetHealth()
    {
        GameManager.Instance.Health = GameManager.Instance.MaxHealth;
    }

    public void DecreaseHealth()
    {
        if (GameManager.Instance.Health > 0)
            GameManager.Instance.Health--;
        EventsManager.Instance.OnHealthChanged.Invoke(GameManager.Instance.Health);
    }

    public void OnHealthChanged(int health)
    {
        UpdateHealthUI(health);
    }

    private void UpdateHealthUI(int health)
    {
        ClearUI();
        for (int i = 0; i < health; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
    }
    private void ClearUI()
    {
        foreach (Transform t in transform)
        {
            t.gameObject.SetActive(false);
        }
    }
}
