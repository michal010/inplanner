﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class contains Cannon logics.
/// </summary>
public class CannonBehaviour : MonoBehaviour
{
    public Transform CannonRifleRef;
    public Transform ShootPointRef;
    public GameObject CannonBallPrefab;

    // Start is called before the first frame update
    void Start()
    {
        EventsManager.Instance.OnAngleChanged.AddListener(OnAngleChanged);
        EventsManager.Instance.OnCannonFired.AddListener(FireCannon);
    }

    private void OnAngleChanged(float angle)
    {
        CannonRifleRef.localRotation = Quaternion.Euler(0, angle, 0);
        Vector3 targetForward = CannonRifleRef.localRotation * Vector3.forward;
        Vector3 targetUp = CannonRifleRef.localRotation * Vector3.up;

    }

    private void LateUpdate()
    {
        //Debuging purposes only.
        ////*-1f because cannon is flipped
        //Vector3 direction = CannonRifleRef.transform.right * -1f;
        //Vector3 directionVector = new Vector3(0, Mathf.Sin(Mathf.Deg2Rad * GameManager.Instance.Angle), Mathf.Cos(Mathf.Deg2Rad * GameManager.Instance.Angle));
        //Debug.DrawLine(CannonRifleRef.transform.position, direction* 5f, Color.red, 5f);
    }
    public void FireCannon()
    {
        CannonBall cannonBall = Instantiate(CannonBallPrefab, ShootPointRef.position, Quaternion.identity, null).GetComponent<CannonBall>();
        Vector2 direction = CannonRifleRef.transform.right * -1f; 
        cannonBall.SetVariables(direction * GameManager.Instance.Power, GameManager.Instance.Angle, 5f);
        cannonBall.Fire();
    }
}
