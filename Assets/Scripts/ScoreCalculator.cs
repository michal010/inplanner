﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for scores values and it's calculations.
/// </summary>
public static class ScoreCalculator
{
    //each zone has r = 0.16, d = 0.32
    private static int[] scores = new int[] { 100,80,60,50,40,30,20,10,5};
    private static float r = 0.16f;
    /// <summary>
    /// Gets score based on distance between two points. Order of params doesn't matter.
    /// </summary>
    /// <param name="targetPos"></param>
    /// <param name="cannonBallPos"></param>
    /// <returns></returns>
    public static int GetScore(Vector2 targetPos, Vector2 cannonBallPos)
    {
        float distance = Vector2.Distance(targetPos, cannonBallPos);

        //0.16*9 = 1,44 - lowest score possible
        int scoreTreshold = Mathf.FloorToInt(distance / r);

        if (scoreTreshold > 8)
            return 0;
        else
            return scores[scoreTreshold];
    }
}
