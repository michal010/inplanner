﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for testing balistic trajectory.
/// </summary>
public class TrajectoryTester : MonoBehaviour
{
    public Vector2 velocity0;
    public float angle;

    private bool fired = true;

    BallisticTrajectory bt;
    // Start is called before the first frame update
    void Start()
    {
        bt = new BallisticTrajectory(velocity0, angle, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y));
    }

    public void Fire()
    {
        fired = true;
    }

    float t = 0;
    // Update is called once per frame
    void Update()
    {
        if (!fired)
            return;
        t += Time.deltaTime;
        transform.position = bt.GetPositionAtTime(t);

        Debug.Log(bt.GetPositionAtTime(t));
    }
}
