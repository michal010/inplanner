﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    BallisticTrajectory bt;
    Vector2 velocity;
    float angle;
    float lifeSpan;
    bool fired = false;
    bool isGrounded = false;

    /// <summary>
    /// Sets initial variables of the class since you can't pass them via constructor since it's monobehaviour
    /// </summary>
    /// <param name="velocity"></param>
    /// <param name="angle"></param>
    /// <param name="lifeSpan"></param>
    public void SetVariables(Vector2 velocity, float angle, float lifeSpan)
    {
        this.velocity = velocity;
        this.angle = angle;
        this.lifeSpan = lifeSpan;
    }

    public void Fire()
    {
        fired = true;
    }
    void Start()
    {
        bt = new BallisticTrajectory(velocity, angle, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y));
    }
    float t = 0;
    void Update()
    {
        if (!fired)
            return;
        t += Time.deltaTime;
        if(t >= lifeSpan)
        {
            Destroy(transform.gameObject);
        }
        if (!isGrounded)
        {
            transform.position = bt.GetPositionAtTime(t);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("collided with " + collision.gameObject.name);
        isGrounded = true;
        float score = ScoreCalculator.GetScore(collision.transform.position, transform.position);
        EventsManager.Instance.OnCannonBallHitTarget.Invoke(score);
    }

}
