﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Events
[System.Serializable]
public class SingleFloatEvent : UnityEvent<float>
{

}

[System.Serializable]
public class SingleIntEvent : UnityEvent<int>
{

}
/// <summary>
/// Class responsible for holding all games' events.
/// </summary>
public class EventsManager : MonoBehaviour
{
    public static EventsManager Instance;

    #region Events
    public SingleFloatEvent OnAngleChanged;
    public SingleFloatEvent OnPowerChanged;
    public SingleFloatEvent OnDistanceChanged;
    public SingleFloatEvent OnCannonBallHitTarget;
    public SingleIntEvent OnHealthChanged;
    public UnityEvent OnCannonFired;
    public UnityEvent OnZeroLivesLeft;
    #endregion

    void Start()
    {
        if(Instance==null)
        {
            Instance = this;
            OnAngleChanged = new SingleFloatEvent();
            OnPowerChanged = new SingleFloatEvent();
            OnDistanceChanged = new SingleFloatEvent();
            OnCannonBallHitTarget = new SingleFloatEvent();
            OnCannonFired = new UnityEvent();
            OnHealthChanged = new SingleIntEvent();
            OnZeroLivesLeft = new UnityEvent();
        }
        else
        {
            Destroy(this);
        }
    }
}
