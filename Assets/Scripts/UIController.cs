﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;


/// <summary>
/// Class resposible for updating UI content and triggering UI buttons' events.
/// </summary>
public class UIController : MonoBehaviour
{
    public TextMeshProUGUI AngleText;
    public TextMeshProUGUI PowerText;
    public TextMeshProUGUI DistanceText;
    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI FireText;
    public bool ImageTargetActive = false;
    public static UIController Instance;

    private Animator UIAnimator;
    private bool FireButtonLocked = false;
    // Start is called before the first frame update
    void Start()
    {
        if(Instance == null)
        {
            Instance = this;
            EventsManager.Instance.OnCannonBallHitTarget.AddListener(OnCannonBallhitTarget);
            EventsManager.Instance.OnZeroLivesLeft.AddListener(OnZeroLivesLeft);
            UIAnimator = GetComponent<Animator>();
        }
        else
        {
            Destroy(this);
        }
    }

    private void OnZeroLivesLeft()
    {
        string scoreString = ScoreText.text + "\n Best score: " + GameManager.Instance.HighScore.ToString() + "\n Press Fire to play again!";
        UpdateText(ref ScoreText, scoreString);
    }

    private void OnCannonBallhitTarget(float score) 
    {
        string scoreString = score.ToString() + " points!"; 
        if(score > GameManager.Instance.HighScore)
        {
            GameManager.Instance.HighScore = (int)score;
        }

        UpdateText(ref ScoreText, scoreString);
        UIAnimator.SetTrigger("Show");

        if(!HeathController.Instance.HasHealthLeft())
        {
            EventsManager.Instance.OnZeroLivesLeft.Invoke();
        }
    }

    private void UpdateText(ref TextMeshProUGUI TMPText, string text)
    {
        TMPText.text = text;
    }

    public void IncreaseAngle()
    {
        if(GameManager.Instance.Angle < 90)
            GameManager.Instance.Angle += 1;
        UpdateText(ref AngleText, GameManager.Instance.Angle + "°");
        EventsManager.Instance.OnAngleChanged.Invoke(GameManager.Instance.Angle);
    }
    public void DecreaseAngle()
    {
        if (GameManager.Instance.Angle > 0)
            GameManager.Instance.Angle -= 1;
        UpdateText(ref AngleText, GameManager.Instance.Angle + "°");
        EventsManager.Instance.OnAngleChanged.Invoke(GameManager.Instance.Angle);
    }

    public void IncreasePower()
    {
        if (GameManager.Instance.Power < 100)
            GameManager.Instance.Power += 1;
        UpdateText(ref PowerText, GameManager.Instance.Power.ToString());
        EventsManager.Instance.OnPowerChanged.Invoke(GameManager.Instance.Power);
    }
    public void DecreasePower()
    {
        if (GameManager.Instance.Power > 0)
            GameManager.Instance.Power -= 1;
        UpdateText(ref PowerText, GameManager.Instance.Power.ToString());
        EventsManager.Instance.OnPowerChanged.Invoke(GameManager.Instance.Power);
    }

    public void IncreaseDistance()
    {
        if (GameManager.Instance.Distance < 50)
            GameManager.Instance.Distance += 1;
        UpdateText(ref DistanceText, GameManager.Instance.Distance.ToString() + " m");
        EventsManager.Instance.OnDistanceChanged.Invoke(GameManager.Instance.Distance);
    }
    public void DecreaseDistance()
    {
        if (GameManager.Instance.Distance > 0)
            GameManager.Instance.Distance -= 1;
        UpdateText(ref DistanceText, GameManager.Instance.Distance.ToString() + " m");
        EventsManager.Instance.OnDistanceChanged.Invoke(GameManager.Instance.Distance);
    }
    public void Fire()
    {
        if (FireButtonLocked || !ImageTargetActive)
            return;
        if (!HeathController.Instance.HasHealthLeft())
        {
            GameManager.Instance.HighScore = 0;
            GameManager.Instance.Score = 0;
            GameManager.Instance.Health = GameManager.Instance.MaxHealth;
            EventsManager.Instance.OnHealthChanged.Invoke(GameManager.Instance.Health);
        }
        else
        {
            FireButtonLocked = true;
            StartCoroutine(FireDelay(4f));
            EventsManager.Instance.OnCannonFired.Invoke();

        }
    }

    private IEnumerator FireDelay(float time)
    {
        float elapsed = 0;
        while(elapsed < time)
        {
            elapsed += Time.deltaTime;
            UpdateText(ref FireText, (time - elapsed).ToString("F2"));
            yield return new WaitForEndOfFrame();
        }
        UpdateText(ref FireText, "Fire");
        FireButtonLocked = false;
        yield return null;
    }
}
